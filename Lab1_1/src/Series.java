// импортируем необходимые классы
import java.util.InputMismatchException;
import java.util.Scanner;

public class Series {
    public static void main(String[] args) {
        // просим пользователя ввести число N
        System.out.print("Введите N: ");
        //пытаемся вычислить сумму ряда при этом проверяя корректность ввода
        try {
            System.out.println(calculateSeries(new Scanner(System.in).nextInt()));
        }
        // если пользователь некорректно ввел число N
        catch (InputMismatchException e) {
            //сообщаем ему о некорректном вводе
            System.out.println("Некорректный ввод");
        }
    }

    public static double calculateSeries(int n) {
        // создаем переменную для хранения частичной суммы ряда
        double sum = 0;
        // для всех i от 1 до N
        for (int i = 1; i <= n; i++) {
            // прибавляем результат выражения 1/i к частичной сумме
            sum += 1.0/i;
        }
        //возвращаем частичную сумму
        return sum;
    }
}
