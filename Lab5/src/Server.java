// импорт необходимых классов
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Random;

public class Server {
    public static void main(String[] args) {
        // создание необходимых полей для контроля игровых параметров на стороне сервера
        ArrayList<String> cards1 = new ArrayList<>();
        ArrayList<String> cards2 = new ArrayList<>();
        int player1Score = 0, player2Score = 0;
        int currentCardRank;
        // данные поля нужны для хранения сообщений от клиента
        String msg1, msg2 ;

        // создаем поля типа Socket для хранения информации о клиентах
        Socket client1, client2;
        // обработка IOException
        try {
            // создаем сервер, порт: 8030
            ServerSocket server = new ServerSocket(8030);
            // выводим в консоль сервера информацию о его успешном запуске (для отладки)
            System.out.println("Сервер запущен по адресу: " + server.getInetAddress());
            //ожидание подключения первого клиента и созранение информации о нем в поле client1
            client1 = server.accept();
            /* создание объекта типа PrintStream на основе потока вывода в консоль первого клиента
            для отправки сообщений (с автоматическим освобождением буфера и заданной кодировкой)*/
            PrintStream ps1 = new PrintStream(client1.getOutputStream(), true, "cp1251");
            /* создание объекта типа BufferedReader на основе потока ввода c консоли первого клиента
            для получения сообщений от него */
            BufferedReader br1 = new BufferedReader(new InputStreamReader(client1.getInputStream()));
            // отправка первому клиенту приветственного сообщения
            ps1.println( "Добро пожаловать в игру! Вам выдана первая карта.");
            //ожидание подключения второго клиента и созранение информации о нем в поле client1
            client2 = server.accept();
            /* создание объекта типа PrintStream на основе потока вывода в консоль второго клиента
            для отправки сообщений (с автоматическим освобождением буфера и заданной кодировкой) */
            PrintStream ps2 = new PrintStream(client2.getOutputStream(), true, "cp1251");
            /* создание объекта типа BufferedReader на основе потока ввода c консоли второго клиента
            для получения сообщений от него*/
            BufferedReader br2 = new BufferedReader(new InputStreamReader(client2.getInputStream()));
            // отправка второму клиенту приветственного сообщения
            ps2.println("Добро пожаловать в игру! Вам выдана первая карта.\"");

            // создание объекта типа Random для генерации псевдослучайных чисел
            Random random = new Random();
            // бесконечный игровой цикл
            while (true) {
                /* для обоих клиентов генерируем карту с псевдослучайным рангом, добавляем в список
                карт каждого игрока строковое значение сгенерированной карты (отдельный метод getCardName()
                преобразует 1 в "Туз", 2 - в "Валет", 3 - в "Даму" и 4 - в "Короля") */
                currentCardRank = 1 + random.nextInt(10);
                cards1.add(getCardName(currentCardRank));
                player1Score += currentCardRank;
                currentCardRank = 1 + random.nextInt(10);
                cards2.add(getCardName(currentCardRank));
                player2Score += currentCardRank;

                // отправляем обоим клиентам сообщение со списком карт на руках у каждого (отдельный метод showCards())
                ps1.println("У вас на руках: " + showCards(cards1) + ".");
                ps2.println("У вас на руках: " + showCards(cards2) + ".");

                // если первый игрок набрал 21 очко, а счет второго превышает 21
                if (player1Score == 21 || player2Score > 21) {
                    /* первому игроку отправляем сообщение о его победе и счете, второго игрока
                    уведомляем о победе первого */
                    ps1.println(String.format("Вы победили со счетом %s.", player1Score));
                    ps1.println("Игра окончена!");
                    ps2.println(String.format("Победил Игрок 1 со счетом %s.", player1Score));
                    ps2.println("Игра окончена!");
                    // выходим из игрового цикла
                    break;
                }
                // если второй игрок набрал 21 очко, а счет первого превышает 21
                else if (player2Score == 21 || player1Score > 21) {
                     /* второму игроку отправляем сообщение о его победе и счете, первого игрока
                    уведомляем о победе второго */
                    ps1.println(String.format("Победил Игрок 2 со счетом %s.", player2Score));
                    ps1.println("Игра окончена!");
                    ps2.println(String.format("Вы победили со счетом %s.", player2Score));
                    ps2.println("Игра окончена!");
                    // выходим из игрового цикла
                    break;
                }

                // отправляем обоим клиентам сообщение с вопросом
                ps1.println("Желаете ли Вы взять еще одну карту?(y/n)");
                ps2.println("Желаете ли Вы взять еще одну карту?(y/n)");

                // получаем от обоих клиентов сообщение (ответ на вопрос)
                msg1 = br1.readLine();
                msg2 = br2.readLine();

                // если один из клиентов отказался брать еще одну карту
                if (msg1.equals("n") || msg2.equals("n")) {
                    // если игроки набрали одинаковое количество очков
                    if (player1Score == player2Score || player1Score > 21 && player2Score > 21) {
                        // отправляем обоим сообщение о ничье
                        ps1.println("Ничья.");
                        ps1.println("Игра окончена!");
                        ps2.println("Ничья.");
                        ps2.println("Игра окончена!\"");
                    }
                    // если у одного игрока больше очков чем у другого
                    else {
                        // определяем победителя по количеству очков ("Игрок 1" или "Игрок 2")
                        String winner = player1Score > player2Score ? "Игрок 1" : "Игрок 2";
                        // определяем счет победителя
                        Integer winnerScore = player1Score > player2Score ? player1Score : player2Score;
                        // отправляем обоим игрокам сообщение с результатами игры, именем и количеством очков победителя
                        ps1.println(String.format("Победил %s со счетом %s.", winner, winnerScore.toString()));
                        ps1.println("Игра окончена!");
                        ps2.println(String.format("Победил %s со счетом %s.", winner, winnerScore.toString()));
                        ps2.println("Игра окончена!");
                    }
                    // в любом из двух случаев, если игрок отказался взять карту, то игровой цикл прерывается
                    break;
                }
            }

            // разрываем соединение с обоими клиентами
            client1.close();
            client2.close();
        }
        // если "поймали" IOException
        catch (IOException e) {
            // уведомляем пользователя об ошибке на стороне сервера (вывод в консоль сервера без отправки сообщений)
            System. out.println( "Ошибка ввода/вывода на стороне сервера: " + e);
        }
    }

    // отдельный метод для получения строки со списком карт (преобразование коллекции строк встроку)
    private static String showCards(ArrayList<String> cards) {
        // создаем объект типа StringBuilder, представляющий пока что пустую результирующую строку
        StringBuilder sb = new StringBuilder();
        // проходя по коллекции строк
        for (String card : cards) {
            // добавляем к результирующей строке строку из коллекции
            sb.append(card + " ");
        }
        // возвращаем результирующую строку без пробела в конце (метод trim())
        return  sb.toString().trim();
    }

    // отдельный метод для преобразования числового значения ранга карты в ее игровое имя
    private static String getCardName(Integer cardRank) {
        // выбор из вариантов значений параметра cardRank
        switch (cardRank) {
            case 1: return "\"Туз\"";
            case 2: return "\"Валет\"";
            case 3: return "\"Дама\"";
            case 4: return "\"Король\"";
            // для карт с рангом от 5 до 10 выводим строковый эквивалент числового значения ранга
            default: return String.format("\"%s\"", cardRank.toString());
        }
    }
}