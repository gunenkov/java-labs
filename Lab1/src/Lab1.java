//импортируем необходимые классы
import java.io.*;
import java.util.Scanner;

public class Lab1 {
    public static String readTextFromFile(String filename) {
        //обрабатываем возможность несуществования считываемого файла
        try {
            /* создаем экземпляр класса Scanner для считывания текста из файла;
            в конструктор класса передаем новый объект класса InputStreamReader,
            создаваемый на основе нового потока для чтения из указываемого файла в передаваемой кодировке*/
            Scanner scanner = new Scanner(new InputStreamReader(new FileInputStream(filename), "UTF-8"));

            //создаем и инициализируем строку
            String text = new String();

            /* пока в файле остались непрочитанные наборы символов, разделенные
            пробелом */
            while (scanner.hasNext()) {
                //добавляем этот набор символов к тексту
                text += scanner.nextLine() +'\n';
            }
            //возвращаем считанный текст
            return text;
        }

        // если не удалось прочитать файл
        catch (IOException e) {
            // выводим в консоль сообщение о невозможности открытия файла
            System.out.println("Не получается открыть файл. Программа будет завершена");
            //останавливаем выполнение программу
            System.exit(-1);
            //формально метод должен вернуть String
            return null;
        }
    }

    public static void writeTextToFile(String filename, String text) {
        //обрабатываем исключение
        try {
            // создаем объект класса OutputStreamWriter для записи в файл
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(filename), "UTF-8");

            // выполняем запись текста в буфер потока
            outputStreamWriter.write(text);

            //отправляем содержимое буфера потока в файл
            outputStreamWriter.flush();

            System.out.println("Отредактированный текст записан в файл " + filename);
        }
        catch (IOException e) {
            System.out.println("Произошла ошибка при записи в выходной файл!");
        }
    }

    public static void main(String[] args) {
        //запрашиваем у пользователя абсолютный путь к файлу
        System.out.println("Введите путь к файлу с исходным текстом:");

        /*создаем экземпляр класса Scanner для считывания строки с консоли;
        в конструктор класса передаем ссылку на поток консольного ввода */
        Scanner scanner = new Scanner(System.in);

        //присваиваем переменной text результат выполнения метода readTextFromFile(String)
        String text = readTextFromFile(scanner.nextLine());

        /*с помощью регулярного выражения, заменяем все символы, не являющиеся
        буквами и пробелом на пустую строку */
        text = text.replaceAll("[^A-Za-zА-Яа-я\\s]", "");

        //создаем переменную для хранения текущей позиции в строке
        int iterator = 0;

        //создаем экземпляр класса StringBuilder
        StringBuilder editedText = new StringBuilder();

        //пока не рассмотрен каждый символ в тексте, кроме последнего
        while (iterator < text.length() - 1) {
            //если текущий символ не совпадает с последующим
            if (text.charAt(iterator) != text.charAt( iterator + 1)) {
                //добавляем его в новый текст
                editedText.append(text.charAt(iterator));
                //продолжаем движение по тексту
                iterator++;
            }
            // если текущий символ совпадает с последующим
            else {
                //пока символ повторяется
                while (text.charAt(iterator) == text.charAt( iterator + 1)) {
                    // добавлем его к новому тексту
                    editedText.append(text.charAt(iterator));
                    // продолжаем движение по тексту
                    iterator++;
                }
                // добавляем последний повторяющийся символ
                editedText.append(text.charAt(iterator));
                // продолжаем движение по тексту
                iterator++;
                // вставляем пробел
                editedText.append(' ');
            }
        }
        //ожидаем ввода пути к выходному файлу
        System.out.println("Введите путь к выходному файлу:");
        //выводим новый текст в файл
        writeTextToFile(scanner.nextLine(), editedText.toString().trim());
        //закрываем сканнер
        scanner.close();
    }
}
