// импортируем необходимые классы
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.util.Calendar;

/* класс Main реализует интерфейс Runnable, что позволяет использовать
метод run для запуска цикла обработки событий UI в отдельном потоке */
public class Lab4 implements Runnable  {
    // точка входа в программу
    public static void main(String[] args) {
        // запуск кода UI в потоке AWT
        SwingUtilities.invokeLater(new Lab4());
    }

    // переопределение метода run для реализации интерфейса Runnable
    @Override
    public void run() {
        // создание окна с указанием заголовка
        JFrame mainFrame = new JFrame("My Pretty Clock");
        /* установка стандартной операции, выполняемой при закрытии окна -
        необходимо убедиться, что потом AWT при этом будет закрыт */
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // отключение возможности изменять размер окна с помощью мыши
        mainFrame.setResizable(false);
        // выбор BorderLayout в качестве Layout'а окна
        mainFrame.setLayout(new BorderLayout());

        // создание экземпляра класса ClockPanel (наследника JPanel)
        ClockPanel clockPanel = new ClockPanel();
        // установка желаемого размера панели с часами
        clockPanel.setPreferredSize(new Dimension(800,800));

        // добавление панели с часами на окно
        mainFrame.add(clockPanel, BorderLayout.NORTH);

        // создание новой панели с GridLayout в качестве Layout'а (одна строка, четыре колонки)
        JPanel controlPanel = new JPanel(new GridLayout(1,4));
        /* создание нового шрифта на основе шрифта Verdana, полужирное
        начертание, размер 14pt */
        Font verdanaFont = new Font("Verdana", Font.BOLD, 14);
        // установка желаемого размера панели с кнопками управления
        controlPanel.setPreferredSize(new Dimension(800,50));
        // установка серого в качестве фонового цвета панели с кнопками управления
        controlPanel.setBackground(Color.gray);

        /* создание необходимых кнопок с указанием текста и добавлением обработчика события
        нажатия; по факту добавляется анонимный объект ActionListener с единственным методом
        actionPerformed(ActionEvent e); для минимизации кода объявление данного объекта было
        заменено лямбда-выражением; функции incrementMinutes() и другие реализованы в классе
         ClockPanel и управляют его внутренними переменными*/
        JButton incrementMinutesButton = new JButton("Прибавить минуту");
        incrementMinutesButton.addActionListener(e -> clockPanel.incrementMinutes());
        JButton decrementMinutesButton = new JButton("Отнять минуту");
        decrementMinutesButton.addActionListener(e -> clockPanel.decrementMinutes());
        JButton incrementHoursButton = new JButton("Прибавить час");
        incrementHoursButton.addActionListener(e -> clockPanel.incrementHours());
        JButton decrementHoursButton = new JButton("Отнять час");
        decrementHoursButton.addActionListener(e -> clockPanel.decrementHours());

        // установка ранее созданного шрифта для текта на каждой из кнопок
        incrementMinutesButton.setFont(verdanaFont);
        incrementHoursButton.setFont(verdanaFont);
        decrementMinutesButton.setFont(verdanaFont);
        decrementHoursButton.setFont(verdanaFont);

        // добавление каждой из кнопок на форму (встанут в линию)
        controlPanel.add(incrementMinutesButton);
        controlPanel.add(decrementMinutesButton);
        controlPanel.add(incrementHoursButton);
        controlPanel.add(decrementHoursButton);
        // добавление панели с кнопками управления на окно и крепление ее к нижней границе
        mainFrame.add(controlPanel, BorderLayout.SOUTH);

        /* автоматическая установка размеров окна на основе ранее указанных желаемых размеров
        добавленных панелей */
        mainFrame.pack();
        // помещение окна в центр экрана
        mainFrame.setLocationRelativeTo(null);
        // установка видимости окна в значение true
        mainFrame.setVisible(true);

        // создание таймера с задержкой 50мс
        Timer timer = new Timer(50, e -> clockPanel.repaint());
        // запуск таймера
        timer.start();
    }
}

/* создание собственного класса ClockPanel, наследника JPanel
для переопределения метода paintComponent(Graphics g) */
class ClockPanel extends JPanel {

    // объявление основных полей класса
    // временные параметры
    private double hours;
    private double minutes;
    private double seconds;

    // объект canvas для рисования на панели
    private Graphics2D canvas;
    //объект renderingHints, необходимый для последующего включения сглаживания
    private RenderingHints renderingHints;

    // объявление двух используемых начертаний
    private BasicStroke thinStroke;
    private BasicStroke boldStroke;

    // объявление основных элементов циферблата
    private Polygon minuteHand;
    private Polygon hourHand;
    private Line2D secondHand;
    private Line2D shortLine;
    private Line2D longLine;

    // объявление шрифта
    private Font verdanaFont;
    /* объявление поля типа FontMetrics для последующего хранения ширины и высоты
    текста в единицах пространства экрана (в пикселях) */
    private FontMetrics fontMetrics;

    // конструктор класса
    public ClockPanel () {
        // выполнение конструктора родителя, так как конструктора по умолчанию более не существует
        super();
        // установка сглаживания в значение "ВКЛЮЧЕНО" с использованием объекта типа RenderingHints
        renderingHints = new RenderingHints( RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        // инициализация используемых начертаний (объектов типа Stroke) с указанием толщины
        boldStroke = new BasicStroke(20);
        thinStroke = new BasicStroke(3);

        /* создание стрелок в виде полигонов с четырьмя вершинами,
        первый массив - x координаты, второй - y координаты вершин */
        minuteHand = new Polygon(new int []{390, 398, 402, 410},new int[]{400, 100, 100, 400},4);
        hourHand = new Polygon(new int []{390, 398, 402, 410},new int[]{400, 190, 190, 400},4);
        // инициализация секундной стрелки как объекта Line2D, с указанием координат концевых точек
        secondHand = new Line2D.Double(400, 400, 400, 80);

        // создание объекта шрифта
        verdanaFont = new Font("Verdana", Font.PLAIN, 60);

        // создание декоративных штрихов на циферблате
        shortLine = new Line2D.Double(400, 105, 400, 95);
        longLine = new Line2D.Double(400, 120, 400, 95);

        /* создание объекта типа Calendar и инициализация его значением системного времени */
        Calendar calendar = Calendar.getInstance();
        // установка временных параметров
        /* Замечание: Math.PI/30 эквивалентно 2*Math.PI/60 и
        Math.PI / 6 эквивалентно 2*Math.PI / 12 */
        seconds = (calendar.get(Calendar.SECOND)) * Math.PI / 30;
        minutes = (calendar.get(Calendar.MINUTE)* Math.PI / 30 + (double) (calendar.get(Calendar.SECOND))/60*Math.PI/30);
        hours = calendar.get(Calendar.HOUR) % 12 * Math.PI / 6 + (double) (calendar.get(Calendar.MINUTE))/60*Math.PI/6;
    }

    //функции, изменяющие временные параметры (вызываются в обработчиках нажатия кнопок)
    public void incrementMinutes() {
        this.minutes += Math.PI/30;
    }

    public void decrementMinutes() {
        this.minutes -= Math.PI/30;
    }

    public void incrementHours() {
        this.hours += Math.PI/6;
    }

    public void decrementHours() {
        this.hours -= Math.PI/6;
    }

    // переопределение метода paintComponent для рисования на панели
    @Override
    public void paintComponent(Graphics graphics) {
        // получение "холста" панели и явное приведение его к типу Graphics2D
        canvas = (Graphics2D)  graphics;
        // применение настроек визуализации (включение сглаживания)
        canvas.setRenderingHints(renderingHints);
        // обязательный вызов метода родителя
        super.paintComponent(canvas);

        // установка темно-серого в качестве используемого цвета
        canvas.setColor(Color.DARK_GRAY);
        // установка необходимого начертания
        canvas.setStroke(boldStroke);
        //рисование основной окружности
        /* Замечание: центр панели - (400, 400); все объекты позиционируются
        по левому верхнему углу */
        canvas.drawOval(60, 60, 680,680);

        // установка необходимого начертания
        canvas.setStroke(thinStroke);
        /* создание объекта для установки параметров афинного преобразования
        (в данном случае поворота); угол - значение seconds; точка опоры - центр панели */
        AffineTransform transforms = AffineTransform.getRotateInstance(seconds, 400, 400);
        /* выполнение афинногго преобразования объекта secondHand с последующим рисованием
        (поворот секундной стрелки) */
        canvas.draw(transforms.createTransformedShape(secondHand));

        // поворот минутной стрелки
        transforms = AffineTransform.getRotateInstance(minutes, 400, 400);
        canvas.fill(transforms.createTransformedShape(minuteHand));

        // поворот часовой стрелки
        transforms = AffineTransform.getRotateInstance(hours, 400, 400);
        canvas.fill(transforms.createTransformedShape(hourHand));

        // установка шрифта Verdana для текста
        canvas.setFont(verdanaFont);
        // получение метрик текущего шрифта (Verdana)
        fontMetrics = canvas.getFontMetrics();
        // установка необходимого начертания
        canvas.setStroke(boldStroke);
        //цикл для отрисовки чисел на циферблате
        for (int i = 12; i > 0; i--)
        {
            /* рисование объекта String на панели с учетом системы координат - объекты
            позиционируются по левому верхнему углу, вследствии чего дополнительно
            определяется середина объекта (на основе метрик шрифта) */
            canvas.drawString(String.valueOf(i), (int)(400 - fontMetrics.stringWidth(String.valueOf(i))/2 +
                    232 * Math.cos(3*Math.PI/2 + Math.PI*i/6)), (int)(400 + fontMetrics.getHeight()/3 +
                    232 * Math.sin(3*Math.PI/2 + Math.PI*i/6)));
        }

        // установка необходимого начертания
        canvas.setStroke(thinStroke);
        // цикл для отрисовки длинных штрихов
        for (int i = 0; i < 60; i++)
        {
            // настройка афинного преобразования и последующая отрисовка преобразованной линии
            transforms = AffineTransform.getRotateInstance(i*Math.PI/30, 400, 400);
            canvas.draw(transforms.createTransformedShape(shortLine));
        }
        // цикл для отрисовки коротких штрихов
        for (int i = 0; i < 12; i++)
        {
            // настройка афинного преобразования и последующая отрисовка преобразованной линии
            transforms = AffineTransform.getRotateInstance(i*Math.PI/6, 400, 400);
            canvas.draw(transforms.createTransformedShape(longLine));
        }

        //выбор светло-серого в качестве используемого цвета
        canvas.setColor(Color.LIGHT_GRAY);
        // рисование небольшой окружности, перекрывающей части стрелок, в центре экрана
        canvas.fillOval(388, 388, 24,24);

        /* изменение значений временных параметров с учетом задержки таймера
        (приращение вычислено с учетом задержки таймера: 50мс = 1/20 секунды);
        при этом секундная стрелка за секунду проходит 2pi/60 радиан; за 50 мс - 2pi/1200 радиан */
        seconds += 2*Math.PI/1200;
        minutes += 2*Math.PI/72000;
        hours += 2*Math.PI/4320000;
    }
}
